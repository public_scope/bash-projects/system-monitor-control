# System Monitor Control
A script to simply log high utilization on a system with no dependancy applications

## Installation
Installation needs to be ran as privileged user.
* Copy this file to /bin/
    * ``` cp sysmonctl /bin/ ```
* Give the file excutable permissions
    * ```chmod +x /bin/sysmonctl```
* Copy the sysload.service to the /etc/systemd/system
    *  ```cp sysload.service /etc/systemd/system```
* Copy the sysmonctl.logrotate to /etc/logrotate.d/
    *  ```cp sysmonctl.logrotate /etc/logrotate.d/```
* Rename the /etc/logrotate.d/sysmonctl.logrotate to /etc/logrotate.d/sysmonctl
    *  ```mv /etc/logrotate.d/sysmonctl.logrotate /etc/logrotate.d/sysmonctl```
* Reload the systemd daemon
    *  ```systemctl daemon-reload```
* Enable the monitor to start on boot
    *  ```systemctl enable sysload```
* Start the process
    *  ```systemctl start sysload```
#
# To use:
* ```tail -f /var/log/systemctl/systemload.log```